package com.bgiaa.flash_card_android.api;

import java.util.List;

public class QuestionWrapperDifficulty {
    public List<AnswersData> answers;
    public String image;
    public String difficulty;
}
