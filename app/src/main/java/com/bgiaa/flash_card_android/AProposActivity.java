package com.bgiaa.flash_card_android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AProposActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);

        String version = BuildConfig.VERSION_NAME;
        TextView versionName = findViewById(R.id.Version);

        versionName.setText(version);

    }
}
